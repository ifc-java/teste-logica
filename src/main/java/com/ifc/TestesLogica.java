package main.java.com.ifc;

import java.util.Scanner;

public class TestesLogica {

    public static void main(String[] args) {
        System.out.println("Olá, mundo");
        CalculaMedia();
        CalculoAnoNascimento();
        CalculoSalario();
    }

    public static void CalculaMedia () {
        Scanner s = new Scanner(System.in);
        float nota1; float nota2; float nota3; float media;

        System.out.println("Digite a nota 1:");
        nota1 = s.nextFloat();

        System.out.println("Digite a nota 2:");
        nota2 = s.nextFloat();

        System.out.println("Digite a nota 3:");
        nota3 = s.nextFloat();

        media = (nota1 + nota2 + nota3) / 3;

        if(media >= 6) {
            System.out.println("APROVADO ");
        }
        else {
            System.out.println("REPROVADO ");
        }

        System.out.println("A média do aluno é:" + media);
    }

    public static void CalculoAnoNascimento () {
        Scanner s = new Scanner(System.in);
        int anoNascimento; int anoAtual; int idade; int meses; int dias; int semanas;

        System.out.println("Digite o ano de nascimento: ");
        anoNascimento = s.nextInt();

        System.out.println("Digite o ano atual: ");
        anoAtual = s.nextInt();

        idade = anoAtual - anoNascimento;
        meses = idade * 12;
        dias = idade * 365;
        semanas = idade * 52;

        System.out.println("A pessoa tem " + idade + " anos.");
        System.out.println("Podemos concluir que ela tem " + meses + " meses.");
        System.out.println("E exatamente " + dias + " dias de vida.");
        System.out.println("Ela possui portanto " + semanas + " semanas.");
    }

    public static void CalculoSalario () {
        Scanner s = new Scanner(System.in);
        float salario; float aumento; float novoSalario;

        System.out.println("Digite o valor do salário: ");
        salario = s.nextFloat();
        System.out.println("Digite o valor do aumento em porcentagem: ");
        aumento = s.nextFloat();

        aumento = salario * (aumento/100);
        novoSalario = aumento + salario;

        System.out.println("O valor do aumento é: " + aumento);
        System.out.println("O valor do novo salário é: " + novoSalario);
    }
}
